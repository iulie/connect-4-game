"use strict";

const PLAYER_RED = "R";
const PLAYER_YELLOW = "Y";
const winner = document.getElementById("winner");
const settingsForm = document.getElementById('game_settings');
let currPlayer = PLAYER_RED;
let gameOver = false;
var rows = "";
var columns = "";
let board = [];
let bottomRows = [];


settingsForm.addEventListener("submit", (e) => {
  e.preventDefault();
  rows = document.getElementById('rows').value;
  columns = document.getElementById('cols').value;
  restartGame();
});

window.onload = function () {
  rows = 6;
  columns = 7;
  setPlayground();
};

function restartGame() {
  removeAllFields();
  setPlayground();
  setWinnerText("");
  gameOver = false;
};


function removeAllFields() {
  const fields = document.querySelectorAll(".field");
  // loop through each div and remove it from the DOM
  fields.forEach(field => {
    field.parentNode.removeChild(field);
  })
};

function setPlayground() {
  let width = 90*columns;
  let height = 90*rows;
  let board = document.getElementById('board');
  board.style.width = width+"px";
  board.style.height = height+"px";
  initializeBoard();
  for (let r = 0; r < rows; r++) {
    for (let c = 0; c < columns; c++) {
      //create fields
      let field = document.createElement("div");         
      field.setAttribute("type", "button");
      field.setAttribute("tabindex", "0");
      //set id for every field
      field.id = r.toString() + "-" + c.toString();
      field.classList.add("field");
      //oncklick start setToken function
      field.addEventListener("click", setToken);
      //put fields on board
      document.getElementById("board").append(field);
    }
  }
}

function initializeBoard() {
  board = [];
  bottomRows = [];
  for (let i = 0; i < columns; i++) {
    bottomRows.push(rows -1);
  }
  for (let r = 0; r < rows; r++) {
    let row = [];
    for (let c = 0; c < columns; c++) {
      row.push(" ");
    }
    board.push(row);
  }
};

function setToken() {
  if (gameOver) {
    return;
  }
  //remove "-" from field.id
  const [r, c] = this.id.split("-").map(Number);
  // figure out which row the current column should be on
  let row = bottomRows[c];
  if (row < 0) return;
  board[row][c] = currPlayer; //update JS board
  const field = document.getElementById(`${row}-${c}`);
  if (currPlayer == PLAYER_RED) {
    field.classList.add("red-piece");
    currPlayer = PLAYER_YELLOW;
  } else {
    field.classList.add("yellow-piece");
    currPlayer = PLAYER_RED;
  }
  field.classList.add("taken");
  row -= 1; //update the row height for that column
  bottomRows[c] = row; //update the array
  checkWinner();
  checkDraw();
}

function checkWinner() {
  checkHorizontal();
  checkVertical();
  checkDiagonalBottomLeftTopRight();
  checkDiagonalTopLeftBottomRight();
}


function checkHorizontal() {
  for (let r = 0; r < rows; r++) {
    for (let c = 0; c < columns - 3; c++) {
      if (board[r][c] != " ") {
        if (
          board[r][c] == board[r][c + 1] &&
          board[r][c + 1] == board[r][c + 2] &&
          board[r][c + 2] == board[r][c + 3]
        ) {
          let winningX = [r, r, r, r];
          let winningY = [c, c + 1, c + 2, c + 3];
          animateWinningFields(winningX, winningY);
          setWinner(r, c);
          return;
        }
      }
    }
  }
}

function checkVertical() {
  for (let c = 0; c < columns; c++) {
    for (let r = 0; r < rows - 3; r++) {
      if (board[r][c] != " ") {
        if (
          board[r][c] == board[r + 1][c] &&
          board[r + 1][c] == board[r + 2][c] &&
          board[r + 2][c] == board[r + 3][c]
        ) {
          let winningX = [r, r + 1, r + 2, r + 3];
          let winningY = [c, c, c, c];
          animateWinningFields(winningX, winningY);
          setWinner(r, c);
          return;
        }
      }
    }
  }
}

function checkDiagonalBottomLeftTopRight() {
  for (let r = 0; r < rows - 3; r++) {
    for (let c = 3; c < columns; c++) {
      if (board[r][c] != " ") {
        if (
          board[r][c] == board[r + 1][c - 1] &&
          board[r + 1][c - 1] == board[r + 2][c - 2] &&
          board[r + 2][c - 2] == board[r + 3][c - 3]
        ) {
          let winningX = [r, r + 1, r + 2, r + 3];
          let winningY = [c, c - 1, c - 2, c - 3];
          animateWinningFields(winningX, winningY);
          setWinner(r, c);
        }
      }
    }
  }
}

function checkDiagonalTopLeftBottomRight() {
  for (let c = 0; c < columns - 3; c++) {
    for (let r = rows - 4; r >= 0; r--) {
      if (board[r][c] != " ") {
        if (
          board[r][c] == board[r + 1][c + 1] &&
          board[r + 1][c + 1] == board[r + 2][c + 2] &&
          board[r + 2][c + 2] == board[r + 3][c + 3]
        ) {
          let winningX = [r, r + 1, r + 2, r + 3];
          let winningY = [c, c + 1, c + 2, c + 3];
          animateWinningFields(winningX, winningY);
          setWinner(r, c);
        }
      }
    }
  }
}

function animateWinningFields(x, y) {
  for (let i = 0; i < 4; i++) {
    let r = x[i];
    let c = y[i];
    let field = document.getElementById(r.toString() + "-" + c.toString());
    field.classList.add("animateWinner");
  }
}

function checkDraw() {
  if (document.querySelectorAll(".taken").length == rows * columns) {
    winner.innerText = "Draw :(";
  }
}

function setWinner(r, c) {
  if (board[r][c] == PLAYER_RED) {
    setWinnerText("Red Wins ! &#128079;");
  } else {
    setWinnerText("Yellow Wins ! &#128079;");
  }
  gameOver = true;
}

function setWinnerText(text) {
  winner.innerHTML = text;
}
